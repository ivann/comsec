# Introduction

La sécurité est un processus et il doit toujours être réévalué. Aucun de ces
outils ne va, comme par magie, rendre une action sécurisée. Ce sont les maillons
d’une chaine qui n’a que la résistance de son élément le plus faible. De plus la
sécurité des communications n’est qu’une partie de la sécurité d’une opération.

Chiffrer une communication ne fait que rendre son contenu illisible lors du
transit entre les participants. Il est toujours possible de savoir qui parle, à
qui, quand, à quelle fréquence… Ce sont les fameuses méta-données qui peuvent
révéler beaucoup d’informations. <sup id="a1">[1](#f1)</sup> Le fait qu’une
communication soit chiffré est, en soit, une méta-donnée, un signal indiquant
que vous jugez cette communication critique. Si vous n’utilisez du chiffrement
que lors de la préparation d’action sensible il devient alors facile de savoir
qu’une action sensible est en préparation. Il est souhaitable d’utiliser ces
outils au quotidien.

De même il est toujours possible d’accéder au contenu de la communication en
accédant à l’appareil d’un des participants.  Se faire arrêter avec son
téléphone déverrouillé peut permettre à la police d’accéder à une conversation
Signal, peut importe le niveau de chiffrement de Signal.

La plupart de ces outils nécessitent un mot de passe. Sachez que vous n’êtes pas
obligé de le donner à la police mais qu’ils peuvent essayer de le forcer. <sup
id="a2">[2](#f2)</sup> C’est pour cela qu’il est important de choisir des mots
de passes uniques et forts mais aussi facile à mémoriser (sinon on a tendance à
compromettre les deux premiers points, ou pire à l’écrire quelque part). Il vaut
mieux créer un long mot de passe facile à mémoriser qu’un mot de passe court
plein de caractères étranges. 4 ou 5 mots aléatoires avec lesquels on peut
imaginer une phrase comme moyen mnémotechnique valent mieux que 3 majuscules, 2
chiffres et 3 caractères spéciaux. <sup id="a3">[3](#f3)</sup>

La meilleurs sécurité reste de tous faire en face à face, c’est très fortement
recommandé pour les actions les plus sensibles. Cependant ce n’est pas toujours
possible, il est parfois nécessaire d’échanger hors d’une réunion, de se
communiquer des informations de dernière minute ou juste pour gagner du temps
lors de l’organisation d’une action peu risquée.


# Outils

## Protonmail

**Qu’est ce que c’est :** Une adresse email chiffré. C’est un compte email comme
un autre mais qui va automatiquement chiffrer les emails envoyé à un autre
compte protonmail.

**Quand l’utiliser :** Pour communiquer par email.

Attention les emails envoyés à des comptes autres que protonmail ne sont pas
chiffrés.

**Pour créer un compte :** https://mail.protonmail.com/create/new?language=fr


## Signal

**Qu’est ce que c’est :** Une messagerie instantanée chiffré. Un équivalent à
Whatsapp ou Telegram sécurisé.

**Quand l’utiliser :** Pour communiquer de manière instantanée.

Pour encore plus de sécurité activez les « messages éphémères », vos messages
seront alors supprimé après que le temps spécifié soit écoulé.

**Pour l’installer :** https://signal.org/fr/download/


## Tor

**Qu’est ce que c’est :** Un navigateur pour être anonyme sur internet.

**Quand l’utiliser :** Pour faire des recherches sur internet lors de la
préparation d’une action.

L’anonymat sur internet est une chose très compliquée à atteindre parfaitement
et à maintenir. S’il est évident que se connecter à son compte Facebook, même à
travers Tor, compromettra votre identité, il y a plein d’autres manières plus
subtiles de se compromettre. Cependant pour des recherches ponctuels en
préparation d’une action cela suffit probablement.

**Pour l’installer :** https://www.torproject.org/fr/download/


## Veracrypt

**Qu’est ce que c’est :** Un logiciel pour chiffrer des fichiers.

**Quand l’utiliser :** Pour stocker des fichiers nécessaire à la préparation d’une
action.

Si Protonmail et Signal sont idéal pour s’échanger des fichiers de manières
sécurisé, une fois ces fichiers reçu il est bon de les chiffrer pour les
stocker. Ainsi ils seront protégés si votre ordinateur est volé ou saisi.
Attention, en cas de saisi la présence d’un dossier chiffré pourra éveiller les
soupçons pour éviter cela il faut se pencher sur le déni plausible. <sup
id="a4">[4](#f4)</sup> Aussi certains programmes utilisés pour lire les fichiers
contenus dans le dossier chiffré peuvent laisser des traces, comme des fichiers
temporaires, hors du dossier chiffré. Une alternative est de chiffrer
intégralement son disque dur.

**Pour l’installer :** https://www.veracrypt.fr/en/Downloads.html


# Pour aller plus loin

* [Guide d’autodéfense numérique](https://guide.boum.org)
* [DEF CON 22 - Zoz - Don't Fuck It Up!](https://www.youtube.com/watch?v=J1q4Ir2J8P8)

Notes :
* <span id="f1">1</span>. [↑](#a1) Michael Hayden, ancien patron de l’agence de
renseignement américaine NSA (National Security Agency), puis de la CIA, déclara
lors d’une conférence universitaire : « Oui, nous tuons des gens en nous basant
sur des métadonnées »
https://www.lemonde.fr/pixels/article/2015/10/20/cree-pour-tuer_4792765_4408996.html
* <span id="f2">2</span>. [↑](#a2) Aucune loi ne punit le fait de ne pas révéler
le mot de passe permettant de déverrouiller son téléphone portable, son
ordinateur, sa tablette ou tout autre support informatique sur lequel figurent
des données en clair.
https://www.nouvelobs.com/rue89/rue89-police-justice/20150205.RUE7744/si-la-police-le-demande-est-on-oblige-de-donner-son-mot-de-passe.html
* <span id="f3">3</span>. [↑](#a3) Un mot de passe tel que "Tr0ub4dor&3" est
mauvais car il est facile pour un logiciel de cassage de mot de passe et
difficile à retenir pour les humains, ce qui conduit à des pratiques peu sûres
comme écrire le mot de passe sur un post-it attaché au moniteur. D'un autre
côté, un mot de passe tel que "chevalexactagrafepile" est difficile à deviner
pour les ordinateurs en raison de son entropie plus importante mais assez facile
à retenir pour les humains.
https://www.explainxkcd.com/wiki/index.php/936:_Password_Strength
https://xkcd.lapin.org/index.php?number=936
* <span id="f4">4</span>. [↑](#a4)
https://fr.wikipedia.org/wiki/VeraCrypt#D%C3%A9ni_plausible
